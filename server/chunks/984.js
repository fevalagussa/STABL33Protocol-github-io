"use strict";
exports.id = 984;
exports.ids = [984];
exports.modules = {

/***/ 7344:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "T": () => (/* binding */ Logo)
/* harmony export */ });
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5675);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__);
 // import { AppConfig } from '../utils/AppConfig';



const Logo = () => {
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("span", {
    className: `text-gray-900 inline-flex items-center `,
    children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx(next_image__WEBPACK_IMPORTED_MODULE_0__["default"], {
      src: "/assets/images/logo.png",
      width: 88,
      height: 80,
      layout: "fixed",
      draggable: false
    })
  });
};



/***/ }),

/***/ 5984:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ Layout)
});

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(968);
var head_default = /*#__PURE__*/__webpack_require__.n(head_);
;// CONCATENATED MODULE: ./src/utils/AppConfig.ts
const AppConfig = {
  site_name: 'Stabl33 Protocol',
  title: 'Stabl33 Protocol',
  description: 'Stabl33 Protocol',
  locale: 'en'
};
// EXTERNAL MODULE: external "next-seo"
var external_next_seo_ = __webpack_require__(6641);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
;// CONCATENATED MODULE: ./src/components/layout/Meta.tsx

 // import { useRouter } from 'next/router';






const Meta = props => {
  // const router = useRouter();
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)((head_default()), {
      children: [/*#__PURE__*/jsx_runtime_.jsx("meta", {
        charSet: "UTF-8"
      }, "charset"), /*#__PURE__*/jsx_runtime_.jsx("meta", {
        name: "viewport",
        content: "width=device-width,initial-scale=1"
      }, "viewport"), /*#__PURE__*/jsx_runtime_.jsx("link", {
        rel: "apple-touch-icon",
        href: `/apple-touch-icon.png`
      }, "apple"), /*#__PURE__*/jsx_runtime_.jsx("link", {
        rel: "icon",
        type: "image/png",
        sizes: "32x32",
        href: `/favicon-32x32.png`
      }, "icon32"), /*#__PURE__*/jsx_runtime_.jsx("link", {
        rel: "icon",
        type: "image/png",
        sizes: "16x16",
        href: `/favicon-16x16.png`
      }, "icon16"), /*#__PURE__*/jsx_runtime_.jsx("link", {
        rel: "icon",
        type: "image/png",
        href: `/favicon-48x48.png`
      }, "favicon")]
    }), /*#__PURE__*/jsx_runtime_.jsx(external_next_seo_.NextSeo, {
      title: props.title,
      description: props.description,
      canonical: props.canonical,
      openGraph: {
        title: props.title,
        description: props.description,
        url: props.canonical,
        locale: AppConfig.locale,
        site_name: AppConfig.site_name
      }
    })]
  });
};


// EXTERNAL MODULE: ./src/components/Logo.tsx
var Logo = __webpack_require__(7344);
// EXTERNAL MODULE: external "@usedapp/core"
var core_ = __webpack_require__(9439);
;// CONCATENATED MODULE: ./src/components/modal/index.tsx




function Modal({
  showModal,
  setShowModal
}) {
  const {
    activateBrowserWallet,
    deactivate,
    account
  } = (0,core_.useEthers)();
  return /*#__PURE__*/jsx_runtime_.jsx(jsx_runtime_.Fragment, {
    children: showModal ?
    /*#__PURE__*/

    /* Overlay Effect */
    jsx_runtime_.jsx("div", {
      className: `fixed block inset-0 bg-[#9e7b30] bg-opacity-80 overflow-y-auto h-full w-full z-20 text-white font-smpx px-4`,
      children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "relative p-5 mx-auto bg-[#AC7635] rounded-[60px] shadow-lg top-20 w-[90%] tablet1:w-[30%] text-center",
        children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "text-[24px] tablet1:text-[36px] flex justify-between",
          children: [/*#__PURE__*/jsx_runtime_.jsx("span", {
            children: "Connect Wallet"
          }), /*#__PURE__*/jsx_runtime_.jsx("span", {
            className: "cursor-pointer",
            onClick: () => setShowModal(false),
            children: "X"
          })]
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "flex items-center px-4 py-3 mt-3 cursor-pointer tablet1:px-8 align-center bg-[#513D26] rounded-2xl",
          onClick: () => {
            setShowModal(false);
            activateBrowserWallet();
          },
          children: [/*#__PURE__*/jsx_runtime_.jsx("img", {
            src: '/assets/images/metamask.png',
            width: 45,
            height: 40
          }), /*#__PURE__*/jsx_runtime_.jsx("span", {
            className: "text-[32px] ml-5",
            children: "Metamask"
          })]
        }), /*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "text-[20px] tablet1:text-[30px] mt-6",
          children: account ? /*#__PURE__*/jsx_runtime_.jsx("button", {
            onClick: () => {
              setShowModal(false);
              deactivate();
            },
            children: "Logout"
          }) : /*#__PURE__*/jsx_runtime_.jsx("button", {
            onClick: () => setShowModal(false),
            children: "Close"
          })
        })]
      })
    }) : null
  });
}
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
;// CONCATENATED MODULE: ./src/components/navigation/Navbar.tsx









const Navbar = () => {
  const {
    0: showMenu,
    1: setShowMenu
  } = (0,external_react_.useState)(false);
  const {
    0: showModal,
    1: setShowModal
  } = (0,external_react_.useState)(false);
  const {
    0: _isLoaded,
    1: setLoaded
  } = (0,external_react_.useState)(false);
  const {
    account
  } = (0,core_.useEthers)();

  const createItem = (link, title) => {
    return {
      link,
      title
    };
  };

  const navItemList = [createItem('/acquire', 'ACQUIRE'), createItem('/earn', 'EARN'), createItem('/borrow', 'BORROW'), createItem('/exchange', 'EXCHANGE'), createItem('/blog', 'BLOG')];

  const ToggleButton = () => /*#__PURE__*/jsx_runtime_.jsx("div", {
    onClick: () => {
      setShowMenu(!showMenu);
    },
    className: "block tablet1:hidden bg-togglebutton bg-cover bg-repeat-round float-right ml-3 w-[60px] h-[36px] my-auto cursor-pointer"
  });

  const ConnectWallet = () => /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: "bg-connectwallet bg-cover bg-repeat-round w-[220px] cursor-pointer font-smpx text-[28px] text-center py-1",
    onClick: () => {
      setShowModal(true);
    },
    children: !account ? 'Connect Wallet' : account.substring(0, 6) + "..." + account.slice(-4)
  });

  const router = (0,router_.useRouter)();
  (0,external_react_.useEffect)(() => {
    setLoaded(true);
  }, []);
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("nav", {
    className: "w-full",
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "flex flex-col mx-auto laptop:flex-row laptop:justify-between laptop:items-center",
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "flex justify-between",
        children: [/*#__PURE__*/jsx_runtime_.jsx(next_link["default"], {
          href: "/",
          children: /*#__PURE__*/jsx_runtime_.jsx("a", {
            children: /*#__PURE__*/jsx_runtime_.jsx(Logo/* Logo */.T, {})
          })
        }), /*#__PURE__*/jsx_runtime_.jsx(ToggleButton, {})]
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: `${showMenu ? 'block mb-10' : 'hidden'} laptop:block`,
        id: "mobile-menu",
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("ul", {
          className: "flex flex-col items-center laptop:items-start laptop:flex-row laptop:self-center text-[36px] font-smpx laptop:gap-[1.4rem] desktop:gap-[3rem]",
          style: {
            WebkitTextStroke: '0.5px black',
            textShadow: '0 1px #987D17, 0 2px #987D17, 0 3px #775A2C'
          },
          children: [navItemList.map((item, index) => /*#__PURE__*/jsx_runtime_.jsx("li", {
            className: router.pathname == item.link ? "active" : "",
            children: /*#__PURE__*/jsx_runtime_.jsx(next_link["default"], {
              href: item.link,
              children: /*#__PURE__*/jsx_runtime_.jsx("span", {
                className: "cursor-pointer",
                children: item.title
              })
            })
          }, index)), /*#__PURE__*/jsx_runtime_.jsx("li", {
            className: "text-center",
            children: /*#__PURE__*/jsx_runtime_.jsx(ConnectWallet, {})
          })]
        })
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx(Modal, {
      showModal: showModal,
      setShowModal: setShowModal
    })]
  });
};


;// CONCATENATED MODULE: ./src/components/background/Background.tsx


const Background = props => /*#__PURE__*/jsx_runtime_.jsx("div", {
  className: (props.image ? props.image : props.color) + " bg-cover px-5 tablet:px-11 tracking-[0.07em] text-white " + props.fontFamily,
  children: props.children
});


;// CONCATENATED MODULE: ./src/components/layout/Layout.tsx









function Layout({
  children
}) {
  const router = (0,router_.useRouter)();
  let bgImg = "";

  switch (router.pathname) {
    case '/':
      bgImg = "bg-mobile tablet1:bg-hero";
      break;

    case '/acquire':
      bgImg = "bg-acquire_mobile tablet1:bg-acquire_desktop";
      break;

    default:
      bgImg = "bg-mobile tablet1:bg-hero";
  }

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
    children: [/*#__PURE__*/jsx_runtime_.jsx((head_default()), {
      children: /*#__PURE__*/jsx_runtime_.jsx(Meta, {
        title: AppConfig.title,
        description: AppConfig.description
      })
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Background, {
      image: bgImg,
      children: [/*#__PURE__*/jsx_runtime_.jsx(Navbar, {}), /*#__PURE__*/jsx_runtime_.jsx("main", {
        className: "pb-[30px]",
        children: children
      })]
    })]
  });
}

/***/ })

};
;